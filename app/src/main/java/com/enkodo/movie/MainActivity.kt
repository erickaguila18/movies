package com.enkodo.movie

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.enkodo.movie.adapter.MovieAdapter
import com.enkodo.movie.model.Movie


const val ID = ""

class MainActivity : AppCompatActivity() {

    private var reciclerView: RecyclerView? = null
    private var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        reciclerView = findViewById(R.id.movies_rv)

        val movies: ArrayList<Movie> = ArrayList()
        movies.add(Movie("1", "Avengers: Endgame", "3h 1m", "April 26, 2019", "https://images-na.ssl-images-amazon.com/images/I/512uVl8PgJL._AC_.jpg"))
        movies.add(Movie("2", "Capitain Marvel", "2h 4m", "March 8, 2019", "https://images-na.ssl-images-amazon.com/images/I/71K3S%2BTk4OL._AC_SL1303_.jpg"))
                reciclerView!!.setHasFixedSize(true)
        layoutManager = LinearLayoutManager(this)
        reciclerView!!.layoutManager = layoutManager
        reciclerView!!.itemAnimator = DefaultItemAnimator()

        reciclerView!!.adapter = MovieAdapter(movies)






    }



}