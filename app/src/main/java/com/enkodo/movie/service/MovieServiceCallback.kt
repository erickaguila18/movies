package com.enkodo.movie.service

import okhttp3.Response

interface MovieServiceCallback {
    fun getMoviesSuccess(response: Response?)

    fun getMoviesError(code: Int, message: String?)

    fun getMoviesFailure()

}