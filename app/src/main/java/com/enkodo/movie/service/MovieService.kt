package com.enkodo.movie.service

import android.util.Log
import okhttp3.*
import java.io.IOException
import java.util.concurrent.TimeUnit


class MovieService : IMoviesService, Callback {
    private val TAG: String = MovieService::class.java.simpleName
    private var mCallback: MovieServiceCallback? = null

    override fun getMoviesData(callback: MovieServiceCallback?, searchString: String?) {
        this.mCallback = callback

        val url: String = java.lang.String.format("", searchString)

        Log.d(TAG, url)
        val request: Request = Request.Builder()
                .url(url)
                .get()
                .build()

        val c: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(59, TimeUnit.SECONDS)
                .readTimeout(59, TimeUnit.SECONDS)
                .build()
        c.newCall(request).enqueue(this)

    }

    override fun onFailure(call: Call, e: IOException) {
        Log.e(TAG, "onFailure: ", e)
        this.mCallback!!.getMoviesFailure()
    }

    override fun onResponse(call: Call, response: Response) {
        if (!response.isSuccessful) {
            Log.e("Services", "onResponse error" + response.code + " " + response.message)
            this.mCallback!!.getMoviesError(response.code, response.message)
        } else {
            Log.e("Services", "onSuccess")
            this.mCallback!!.getMoviesSuccess(response)

        }
    }
}