package com.enkodo.movie.service

interface IMoviesService {
    fun getMoviesData(callback: MovieServiceCallback?, searchString: String?)
}