package com.enkodo.movie.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enkodo.movie.R
import com.enkodo.movie.model.Movie
import com.squareup.picasso.Picasso


class MovieAdapter(data: ArrayList<Movie>) :
    RecyclerView.Adapter<MovieAdapter.MyViewHolder>() {
    private val dataSet: ArrayList<Movie> = data

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.txtTitle)
        var duration: TextView = itemView.findViewById(R.id.txtDuration)
        var date: TextView = itemView.findViewById(R.id.txtDate)
        var image: ImageView = itemView.findViewById(R.id.image_movie)
        var progressBar: ProgressBar = itemView.findViewById(R.id.progress_bar)
        var progressBarText: TextView = itemView.findViewById(R.id.text_view_progress)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.movie_row, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        val title = holder.title
        val duration = holder.duration
        val date = holder.date
        val image = holder.image
        val progressBar = holder.progressBar
        val progressBarText = holder.progressBarText


        progressBar.progress = 86
        progressBarText.text = "86%"

        title.text = dataSet[listPosition].title
        duration.text = dataSet[listPosition].duration
        date.text = dataSet[listPosition].date
        Picasso.get().load(dataSet[listPosition].image).into(image)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

}
