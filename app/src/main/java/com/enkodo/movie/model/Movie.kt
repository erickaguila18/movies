package com.enkodo.movie.model

data class Movie(
    val id: String,
    val title: String,
    val duration: String,
    val date: String,
    val image: String
)
